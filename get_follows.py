#!/usr/bin/env python
from twitchAPI.twitch import Twitch
from twitchAPI.oauth import UserAuthenticator
from twitchAPI.type import AuthScope
from twitchAPI.oauth import refresh_access_token
from twitchAPI.helper import first
import configparser
import os
import asyncio


async def user_refresh(token: str, refresh_token: str):
    print(f'my new user token is: {token}')


async def app_refresh(token: str):
    print(f'my new app token is: {token}')


async def main():

    config_name = "~/.config/twitch-wofi/config.ini"
    target_scope = [AuthScope.USER_READ_FOLLOWS]

    config = configparser.ConfigParser()
    config.read(os.path.expanduser(config_name))

    client_id = config["DEFAULT"]["client_id"]
    client_secret = config["DEFAULT"]["client_secret"]
    username = config["DEFAULT"]["username"]

    try:
        access_token = config["DEFAULT"]["access_token"]
        refresh_token = config["DEFAULT"]["refresh_token"]

    except Exception:
        # The access & refresh tokens were not found,
        # so we assume its never been run before
        access_token, refresh_token = None, None

    twitch = await Twitch(client_id, client_secret, authenticate_app=False)
    twitch.app_auth_refresh_callback = app_refresh
    twitch.user_auth_refresh_callback = user_refresh

    try:
        new_token, new_refresh_token = await refresh_access_token(
                refresh_token,
                client_id,
                client_secret)
    except Exception:
        auth = UserAuthenticator(twitch, target_scope, force_verify=False)
        new_token, new_refresh_token = auth.authenticate()

    await twitch.set_user_authentication(
            new_token,
            target_scope,
            new_refresh_token,
            False)

# We good

    user = await first(twitch.get_users(logins=username))

    streams = twitch.get_followed_streams(user.id, first=50)
    async for live_user in streams:
        user = live_user.user_name
        viewers = live_user.viewer_count
        game = live_user.game_name
        print(f"{user}\n {game}\n {viewers}")

# SAVE THE CONFIG

    config.set("DEFAULT", "access_token", new_token)
    config.set("DEFAULT", "refresh_token", new_refresh_token)

    with open(os.path.expanduser(config_name), 'w') as configfile:
        config.write(configfile)

if __name__ == "__main__":
    asyncio.run(main())
