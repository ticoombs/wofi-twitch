#!/bin/bash 
streamer=$($HOME/bin/get_follows.py | \
    wofi \
    --dmenu \
    --columns 3 \
    --width 30% \
    --height 30% \
    --prompt "Open Twitch Stream?" \
    --cache-file=/dev/null
)
streamlink https://twitch.tv/$streamer --verbose-player
