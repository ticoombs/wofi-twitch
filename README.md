# Wofi-Twitch

Load up a twitch stream via wofi.

1. Create an App  https://dev.twitch.tv/console/apps
2. copy config.ini.example to ~/.config/twitch-wofi/config.ini
3. Populate config.ini with 
  - ClientID
  - SecretID
  - Your twitch username
4. copy twitch-wofi.sh and get_follows.py to ~/bin/
5. install deps `pip install --user twitchAPI`
6. bind your hotkey for running twitch-wofi: 
  - `bindsym $mod+t exec --no-startup-id "$HOME/bin/twitch-wofi.sh"` in sway/i3
7. Run it, and it will pop up in your browser saying to give it access to your Follows
8. Enjoy
